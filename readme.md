Back End Developer Case For Candidate
Times
3 days to doing a Test Case
Instruction
We want to create an app that helps users to track their daily / monthly expenses and
incomes, thus ultimately helping them not to overspend or lose track of their finances. At the
same time, the app needs to be able to inform the users what is the highest expense, what's
left in a month, so that they can improve their expense and income pattern in the future.
Please create Personal Finance Manager API service with minimum
requirements/functionalities below:
- Must have Register, Login and Logout feature
- Able to maintain (Create, update, delete and display) financial accounts
- Able to maintain (Create, update, delete and display) financial transaction based on
financial accounts above with minimum but not limited to fields title, description,
amount, Financial Account, Financial Account Name
- Able to show filter and searching the datas (using pagination)
- All delete function must implementing soft delete function and able to restore the
deleted data
- Able to get user profile data
- Able to show transactions summary for reporting purposes (daily and monthly base)
Specification and limitations
- Please use openAPI standard for REST API both request and response
- All api must implement JWT for API Authentication
- Please use programming best practice solution (such as using design pattern or clean
architecture)
- Use Mysql Database or NoSQL (if possible and don’t forget to add migration script)
- Use Laravel 5.x or Golang or NodeJS
- Code must store on github or bitbucket or gitlab
- Feel free if you want add additional function to provide better experience
UI Design Reference
https://xd.adobe.com/view/a141ddbb-e6d8-4d25-9d43-63f219deef39-f412/