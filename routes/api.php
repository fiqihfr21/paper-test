<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function () {
    //Route for authentication
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::group(['middleware' => 'auth.jwt'], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('me', 'AuthController@getAuthUser');

        //Route for financeAccount
        Route::get('financeAccount', 'FinanceAccountController@index');
        Route::post('financeAccount/search', 'FinanceAccountController@search');
        Route::get('financeAccount/{id}', 'FinanceAccountController@show');
        Route::post('financeAccount', 'FinanceAccountController@store');
        Route::put('financeAccount/{id}', 'FinanceAccountController@update');
        Route::delete('financeAccount/{id}', 'FinanceAccountController@destroy');
        Route::get('trash/financeAccount', 'FinanceAccountController@trash');
        Route::get('restore/{id}/financeAccount', 'FinanceAccountController@restore');
        Route::get('restoreAll/financeAccount', 'FinanceAccountController@restoreAll');

        //Route for financeTransaction
        Route::get('financeTransaction', 'FinanceTransactionController@index');
        Route::post('financeTransaction/search', 'FinanceTransactionController@search');
        Route::get('financeTransaction/{id}', 'FinanceTransactionController@show');
        Route::post('financeTransaction', 'FinanceTransactionController@store');
        Route::put('financeTransaction/{id}', 'FinanceTransactionController@update');
        Route::delete('financeTransaction/{id}', 'FinanceTransactionController@destroy');
        Route::get('trash/financeTransaction', 'FinanceTransactionController@trash');
        Route::get('restore/{id}/financeTransaction', 'FinanceTransactionController@restore');
        Route::get('restoreAll/financeTransaction', 'FinanceTransactionController@restoreAll');

        //Route for transactionSummary
        Route::post('transactionDailySummary', 'DashboardController@transactionDailySummary');
        Route::post('transactionMonthlySummary', 'DashboardController@transactionMonthlySummary');
    });
});
