<?php

use Illuminate\Database\Seeder;
use App\user;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();

        $user->name = 'User';
        $user->email = 'user@paper.id';
        $user->password = Hash::make('123123');
        $user->last_login = Carbon::now();
        $user->save();
    }
}
