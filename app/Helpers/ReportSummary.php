<?php

namespace App\Helpers;

use DB;
use App\Models\FinanceTransaction;
use Auth;

class ReportSummary
{
    public static function dailyTransaction($year, $month)
    {
        $reportsData = FinanceTransaction::with('financeAccount')->whereHas('financeAccount', function ($query) {
            return $query->where('user_id', Auth::user()->id);
        })
            ->selectRaw('DATE(created_at) as date, count(`id`) as count, sum(finance_amount) AS finance_amount')
            ->whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->groupBy('date')
            ->orderBy('date', 'asc')
            ->get();

        $reports = [];
        foreach ($reportsData as $report) {
            $key = substr($report->date, -2);
            $reports[$key] = $report;
            $reports[$key]->finance_amount = $report->finance_amount;
        }

        return $reports;
    }

    public static function monthlyTransaction($year)
    {
        $reportsData = FinanceTransaction::with('financeAccount')->whereHas('financeAccount', function ($query) {
            return $query->where('user_id', Auth::user()->id);
        })
            ->selectRaw('MONTH(created_at) as month, YEAR(created_at) as year, count(`id`) as count, sum(finance_amount) AS finance_amount')
            ->whereYear('created_at', $year)
            ->groupBy('month')
            ->groupBy('year')
            ->orderBy('created_at', 'asc')
            ->get();

        $reports = [];
        foreach ($reportsData as $report) {
            $key = str_pad($report->month, 2, '0', STR_PAD_LEFT);
            $reports[$key] = $report;
            $reports[$key]->finance_amount = $report->finance_amount;
        }

        return $reports;
    }
}
