<?php
namespace App\Helpers;

use App\User;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;

class LogLogin
{
    public static function lastLogin()
    {
        $user = User::where('id', Auth::user()->id)->first();
        $user->last_login = Carbon::now();
        $user->save();
    }
}
