<?php

namespace App\Helpers;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

trait FailedValidation
{
    public function failedValidation(Validator $validator) { 
        throw new HttpResponseException(
          response()->json([
            'messages' => $validator->errors()->all(),
            'status' => 200,
          ], 200)
        ); 
    }
}
