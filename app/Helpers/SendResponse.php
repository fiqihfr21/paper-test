<?php

namespace App\Helpers;

class SendResponse
{
    public static function success($key, $data, $code){
        $response[$key] = $data;
        $response['status'] = $code;
        
        return response()->json($response, $code);
    }

    public static function fail($message, $code){
        $response['message'] = $message;
        $response['status'] = $code;
        
        return response()->json($response,$code);
    }

    public static function error($message, $code){
        $response['message'] = $message;
        $response['status'] = $code;
        
        return response()->json($response,$code);
    }
}