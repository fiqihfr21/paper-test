<?php

namespace App\Http\Controllers;

use App\Helpers\SendResponse;
use App\Models\FinanceTransaction;
use App\Http\Requests\FinanceTransactionRequest;
use Illuminate\Http\Request;
use Auth;

class FinanceTransactionController extends Controller
{
    public function index()
    {
        try {
            $data = FinanceTransaction::with('financeAccount')->whereHas('financeAccount', function ($query) {
                return $query->where('user_id', Auth::user()->id);
            })->latest()->paginate(10);
            return SendResponse::success("financeTransaction", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function search(Request $request)
    {
        try {
            $keyword = $request->keyword;
            $created_at = $request->created_at;
            $finance_name = $request->finance_name;
            $data = FinanceTransaction::with('financeAccount');

            if ($keyword != null) {
                $data = $data->whereHas('financeAccount', function ($query) {
                    return $query->where('account_name',  'like', '%' . request('keyword') . '%');
                })
                    ->orWhere('finance_name', 'like', '%' . $keyword . '%')
                    ->orWhere('finance_amount', 'like', '%' . $keyword . '%')
                    ->orWhere('finance_description', 'like', '%' . $keyword . '%');
            }

            if ($request->finance_name != null) {
                $data = $data->where('account_name', 'like', '%' . $finance_name . '%');
            }

            if ($request->created_at != null) {
                $data = $data->where('created_at', 'like', '%' . $created_at . '%');
            }

            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            } else {
                $data = $data->paginate(10);
                return SendResponse::success("financeTransaction", $data, 200);
            }
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function show($id)
    {
        try {
            $data = FinanceTransaction::with('financeAccount')->find($id);
            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }
            return SendResponse::success("financeTransaction", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function store(FinanceTransactionRequest $request)
    {
        try {
            $data = new FinanceTransaction();
            $data->finance_account_id = $request->finance_account_id;
            $data->finance_name = $request->finance_name;
            $data->finance_amount = $request->finance_amount;
            $data->finance_description = $request->finance_description;
            $data->save();

            return SendResponse::success("financeTransaction", $data, 201);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function update(FinanceTransactionRequest $request, $id)
    {
        try {
            $data = FinanceTransaction::find($id);
            $data->finance_account_id = $request->finance_account_id;
            $data->finance_name = $request->finance_name;
            $data->finance_amount = $request->finance_amount;
            $data->finance_description = $request->finance_description;
            $data->save();

            return SendResponse::success("financeTransaction", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function destroy($id)
    {
        try {
            $data = FinanceTransaction::find($id);
            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }
            $data->delete();
            return SendResponse::success("financeTransaction", "No Content", 204);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function trash()
    {
        try {
            $data = FinanceTransaction::onlyTrashed()->with('financeAccount')->whereHas('financeAccount', function ($query) {
                return $query->where('user_id', Auth::user()->id);
            })->latest()->paginate(10);
            return SendResponse::success("financeTransaction", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function restore($id)
    {
        try {
            $data = FinanceTransaction::onlyTrashed()->with('financeAccount')->whereHas('financeAccount', function ($query) {
                return $query->where('user_id', Auth::user()->id);
            })->where('id', $id)->first();
            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }
            $data->restore();
            return SendResponse::success("message", "Restore Succesful", 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function restoreAll()
    {
        try {
            $data = FinanceTransaction::onlyTrashed()->with('financeAccount')->whereHas('financeAccount', function ($query) {
                return $query->where('user_id', Auth::user()->id);
            });
            $data->restore();
            return SendResponse::success("message", "Restore All Succesful", 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }
}
