<?php

namespace App\Http\Controllers;

use App\Helpers\SendResponse;
use App\Models\FinanceAccount;
use Illuminate\Http\Request;
use App\Http\Requests\FinanceAccountRequest;
use Auth;
use DB;

class FinanceAccountController extends Controller
{
    public function index()
    {
        try {
            $data = FinanceAccount::where('user_id', Auth::user()->id)->latest()->paginate(10);
            return SendResponse::success("financeAccount", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function search(Request $request)
    {
        try {
            $keyword = $request->keyword;
            $account_name = $request->account_name;
            $data = DB::table('finance_account');

            if ($keyword != null) {
                $data = $data->where('account_name', 'like', '%' . $keyword . '%')
                    ->orWhere('account_type', 'like', '%' . $keyword . '%')
                    ->orWhere('account_description', 'like', '%' . $keyword . '%');
            }

            if ($request->account_name != null) {
                $data = $data->where('account_name', 'like', '%' . $account_name . '%');
            }

            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }else{
                $data = $data->paginate(10);
                return SendResponse::success("financeAccount", $data, 200);
            }
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function show($id)
    {
        try {
            $data = FinanceAccount::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }
            return SendResponse::success("financeAccount", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function store(FinanceAccountRequest $request)
    {
        try {
            $data = new FinanceAccount();
            $data->user_id = Auth::user()->id;
            $data->account_name = $request->account_name;
            $data->account_type = $request->account_type;
            $data->account_description = $request->account_description;
            $data->save();

            return SendResponse::success("financeAccount", $data, 201);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function update(FinanceAccountRequest $request, $id)
    {
        try {
            $data = FinanceAccount::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }
            $data->account_name = $request->account_name;
            $data->account_type = $request->account_type;
            $data->account_description = $request->account_description;
            $data->save();

            return SendResponse::success("financeAccount", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function destroy($id)
    {
        try {
            $data = FinanceAccount::where('user_id', Auth::user()->id)->where('id', $id)->first();
            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }
            $data->delete();
            return SendResponse::success("financeAccount", "No Content", 204);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function trash()
    {
        try {
            $data = FinanceAccount::onlyTrashed()->where('user_id', Auth::user()->id)->latest()->paginate(10);
            return SendResponse::success("financeAccount", $data, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function restore($id)
    {
        try {
            $data = FinanceAccount::onlyTrashed()->where('user_id', Auth::user()->id)->where('id', $id)->first();
            if ($data == null) {
                return SendResponse::error("No Data Found", 404);
            }
            $data->restore();
            return SendResponse::success("message", "Restore Succesful", 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function restoreAll()
    {
        try {
            $data = FinanceAccount::onlyTrashed()->where('user_id', Auth::user()->id);
            $data->restore();
            return SendResponse::success("message", "Restore All Succesful", 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }
}
