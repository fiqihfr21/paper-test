<?php

namespace App\Http\Controllers;

use App\Helpers\LogLogin;
use App\Helpers\SendResponse;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    protected $loginAfterSignUp = true;

    public function register(RegisterAuthRequest $request)
    {
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->last_login = Carbon::now();
            $user->save();

            if ($this->loginAfterSignUp) {
                return $this->login($request);
            }

            return SendResponse::error('Invalid Email or Password', 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function login(Request $request)
    {
        try {
            $input = $request->only('email', 'password');
            $jwt_token = null;

            if (!$jwt_token = JWTAuth::attempt($input)) {
                return SendResponse::error("Invalid Email or Password", 401);
            }
            LogLogin::lastLogin();
            return SendResponse::success('token', $jwt_token, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);
            return SendResponse::success('message', 'User logged out successfully', 200);
        } catch (JWTException $exception) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function getAuthUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        try {
            $user = JWTAuth::authenticate($request->token);
            return SendResponse::success('user', $user, 200);
        } catch (JWTException $exception) {
            return SendResponse::fail("Server Error", 500);
        }
    }
}
