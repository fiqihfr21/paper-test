<?php

namespace App\Http\Controllers;

use App\Helpers\ReportSummary;
use App\Helpers\SendResponse;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function transactionDailySummary(Request $request)
    {
        try {
            $year = $request->get('year', date('Y'));
            $month = $request->get('month', date('m'));
            $dailyTransaction = ReportSummary::dailyTransaction($year, $month);
            return SendResponse::success("dailyTransaction", $dailyTransaction, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }

    public function transactionMonthlySummary(Request $request)
    {
        try {
            $year = $request->get('year', date('Y'));
            $monthlyTransaction = ReportSummary::monthlyTransaction($year);
            return SendResponse::success("monthlyTransaction", $monthlyTransaction, 200);
        } catch (\Exception $e) {
            return SendResponse::fail("Server Error", 500);
        }
    }
}
