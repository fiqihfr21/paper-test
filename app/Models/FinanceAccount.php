<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FinanceAccount extends Model
{
    use SoftDeletes;

    protected $table = 'finance_account';

    protected $fillable = [
        'user_id', 'account_name', 'account_type', 'account_description'
    ];

    protected $dates = ['deleted_at'];

}
