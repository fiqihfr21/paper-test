<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FinanceTransaction extends Model
{
    use SoftDeletes;

    protected $table = 'finance_transaction';

    protected $fillable = [
        'finance_account_id', 'finance_name', 'finance_amount', 'finance_description'
    ];

    protected $dates = ['deleted_at'];

    protected $appends = [
        'account_name'
    ];

    public function financeAccount(){
        return $this->belongsTo('App\Models\FinanceAccount','finance_account_id');
    }

    public function getAccountNameAttribute()
    {
        return $this->financeAccount['account_name'];
    }
}
